import memory.Card;
import memory.Image;

public class ThreadDemo {
	
	Card[] cards;

	public ThreadDemo() {
		super();
		Image splashPage = new Image("img/MEMORY.png");
		splashPage.show();
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				cards = new Card[4];
				
				for (int i = 0; i < cards.length; i++) {
					cards[i] = new Card((int)(Math.random()*3));
					cards[i].setPosition(10 + 210*(i%5), 10 + 210*(i/5));
					
					
				}
				
			}
		}).start();
		
		Image lost = new Image("img/LOST.png");
		lost.setPosition(50, 100);
		lost.show();
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new ThreadDemo();

	}

}
