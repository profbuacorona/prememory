import javax.swing.plaf.SliderUI;

import memory.Card;

/*
 * Una carta posizionabile e nascondibile che si gira ad ogni click
 */


public class Test6 {

	public static void main(String[] args) throws InterruptedException {
		
		Card card; //dichiarazione di variabile
		
		card = new Card(Card.HORSE);
		
		card.setPosition(100, 100);
		
		card.show();
		
		Thread.sleep(3000);
		
		System.out.println("hide");
		card.hide();
		

		Thread.sleep(3000);
		
		System.out.println("show");
		card.show();

	}

}
