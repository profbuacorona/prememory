import memory.Image;

//spostamento delle immagini
public class Test4 {

	public static void main(String[] args) {
		
		Image image = new Image("img/dubbioso.jpg");
		Image image2 = new Image("img/reading smiley.png");
		
		image.show();
		image2.show();	
		
		image2.setPosition(100, 100);
		image.setPosition(-200, -200);
	}

}
