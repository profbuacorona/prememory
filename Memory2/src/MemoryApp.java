import memory.Card;
import memory.CardListener;
import memory.ClickListener;
import memory.Image;

public class MemoryApp {

	private Card cards[];

	public MemoryApp() {
		super();
		Image splashPage = new Image("img/MEMORY.png");
		splashPage.show();
		splashPage.register(new ClickListener() {
			
			@Override
			public void click() {
					
				splashPage.hide();
				startGame();
			}

		});
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new MemoryApp();
	}
	
	private void startGame() {
		cards = new Card[6];
		
		cards[0] = new Card(Card.HORSE);
		cards[1] = new Card(Card.HORSE);
		cards[2] = new Card(Card.APPLE);
		cards[3] = new Card(Card.APPLE);
		cards[4] = new Card(Card.FLOWER);
		cards[5] = new Card(Card.FLOWER);
		
		CardListener logic = new Logic();

		for (int i = 0; i < cards.length; i++) {	
			cards[i].setPosition(10 + 210*(i%3), 10 + 210*(i/3));
			cards[i].registerCardListener(logic);
			cards[i].turn();
		}
	}
	
	
	class Logic implements CardListener {
		Card prima;
		int errors;
		@Override
		public void cardTurned(Card card) {
			// TODO Auto-generated method stub
			if(prima == null){
				card.disable();
				prima = card;
			}
			else if (prima.getType() == card.getType()){
				prima = null;
				card.disable();
			}
			else{
				prima.turn();
				card.turn();
				errors++;
				prima = null;
			}
				
		}
		
		
	}
}
