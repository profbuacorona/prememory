import memory.ClickListener;
import memory.Image;


//servizio di notifica 'click sull'immagine'

/*
 * TODO:
 * 
 *  1) null pinter exception
 *  2) notifico solo l'immagine che sta sopra
 *  3) se si clicca su un'immagine nascosta non deve partire la notifica
 */

public class Test5 {
	

	public static void main(String[] args) {
		
		Image image = new Image("img/apple.png");
		Image image2 = new Image("img/back.png");
		
		//image.show();
		image2.show();	
		
		image2.setPosition(100, 100);
		image.setPosition(200, 100);
		

		CallBack callBack = new CallBack("apple");
		CallBack callBack2 = new CallBack("back");
		
		image.register(callBack);
		image2.register(callBack2);
	}

}
