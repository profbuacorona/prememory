package memory;

public class Card {
	
	public static final int HORSE = 0;
	public static final int FLOWER = 1;
	public static final int APPLE = 2;
	

	private Image front;
	private Image back;
	private Image currentImage;
	private int type;
	private boolean su = true;
	private boolean abilitata = true;
	
	private  CardListener cardListener;
	
	public Card(int type) {
		// TODO Auto-generated constructor stub
		
		this.type = type;
		switch(type) {
		
		case HORSE: front = new Image("img/horse.png"); break;
		case FLOWER: front = new Image("img/flower.png"); break;
		case APPLE: front = new Image("img/apple.png"); break;
		
		}
		
		back=new Image("img/back.png");
		currentImage = front;
		front.show();
		
		front.register(new ClickListener() {
			
			@Override
			public void click() {
				if(abilitata){
					front.hide();
					back.show();
					su = false;
					currentImage = back;
					if(cardListener != null)
					  cardListener.cardTurned(Card.this);
				}
			}
		});
		back.register(new ClickListener() {
			
			@Override
			public void click() {
				if(abilitata){
					back.hide();
					front.show();
					su = true;
					currentImage = front;
					cardListener.cardTurned(Card.this);
			 }
			}
		});
		
	}
	
	public void registerCardListener (CardListener cardListener){
		this.cardListener = cardListener;
	}

	public void show() {
		currentImage.show();
	}

	public void hide() {
		currentImage.hide();
	}

	public void setPosition(int x, int y) {
		back.setPosition(x, y);
		front.setPosition(x, y);
	}
	@Override
	public String toString() {
		String ris = "";
		
		switch (type) {
		case HORSE: ris = "Horse"; break;
		case FLOWER: ris = "Flower"; break;
		case APPLE: ris = "Apple"; break;
		} 
		return ris;
	}

	public void disable() {
		abilitata = false;
		
	}
	public void enable(){
		abilitata = true;
	}

	public int getType() {
		return type ;
	}

	public void turn() {
		if(su){
			front.hide();
			back.show();
			su = false;
			currentImage = back;
		}
		else{
			front.show();
			back.hide();
			su = true;
			currentImage = front;
		}
	}


}
