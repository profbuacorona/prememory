package memory;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Image {
	
	private ClickListener listener;
	
	private static JFrame frame; //variabile di classe: unica per tutti gli oggetti
	private static MemoryPanel panel;
	private java.awt.Image img;

	private boolean toShow;
	private static Image[] images;
	private static int counter;
	
	private int x, y;
	private int w, h;
	
	
	public Image(String string) {
		// TODO Auto-generated constructor stub
		
		File file = new File(string);
		try {
			img = ImageIO.read(file );
			w = img.getWidth(null);
			h = img.getHeight(null);
			System.out.println(img + ": " + w + ", " + h);
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//ci dev'essere un solo frame per tutte le immagini
		if(frame == null) {
			frame = new JFrame("memory");
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			panel = new MemoryPanel();
			frame.add(panel);
			frame.setBounds(100, 100, 1000, 600);
			frame.setVisible(true);
			
			images = new Image[100];
			
		}
		
		images[counter] = this;
		counter++;
		
	}

	
	public void show() {
		toShow = true;
		panel.repaint();
		
	}
	
	
	//callback?????
	
	
	class MemoryPanel extends JPanel implements MouseListener {

		
		public MemoryPanel() {
			super();
			
			this.addMouseListener(this);
		}

		@Override
		protected void paintComponent(Graphics g) {
			// TODO Auto-generated method stub
			super.paintComponent(g);
			
			for(int i = 0; i < counter; i++){
				if(images[i].toShow()) g.drawImage(images[i].img, images[i].x, images[i].y, null);	
			}
			
			System.out.println("paint component");
			
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			
			for (int i = counter-1; i >=0; i--) {
				Image image = images[i];
				if(image.contains(e.getX(), e.getY()) && image.listener != null && image.toShow){
					image.listener.click();
					break;
				}
				
			}
			
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		
		
	}


	public void hide() {

		toShow = false;
		panel.repaint();
	}

	boolean contains(int x, int y) {
		int dx = x - this.x;
		int dy = y - this.y;
		return dx > 0 && dx < w  && dy > 0 && dy < h;
	}


	boolean toShow() {
		// TODO Auto-generated method stub
		return toShow;
	}

	public void setPosition(int x, int y) {
		
		this.x = x;
		this.y = y;
		
	}

	public void register(ClickListener listener) {
		
		this.listener = listener;
		
	}

}
