package memory;

public interface CardListener {
	/**
	 *Quando la carta si gira, si attiva il metodo 
	 * @param card = � la carta che viene girata
	 */
	void cardTurned (Card card);
	
	
}
