package memory;


/**
 * Interfaccia d'ascolto ....
 * @author 2015_2016
 *
 */
public interface ClickListener {
	
	/**
	 * Metodo chiamato quando l'utente clicca sulla Image
	 * dove � registrato l'ascoltatore
	 */
	void click();

}
