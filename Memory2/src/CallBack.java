import memory.ClickListener;

//di solito si chiama listener!!
	class CallBack implements ClickListener {
		
		private String message;

		public CallBack(String message) {
			
			this.message = message;
			
		}

		@Override
		public void click() {
			
			System.out.println(message);
			
		}
	
	}